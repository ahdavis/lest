;;;; packages.lisp
;;;; Defines the packages for lest
;;;; Created by Andrew Davis
;;;; Created on 8/27/2019
;;;; MIT licensed

;;; create the package
(defpackage :lest
  (:use :common-lisp)
  (:export :deftest :combine-results :check))

;;; end of file
