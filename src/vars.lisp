;;;; vars.lisp
;;;; Defines global variables for lest
;;;; Created by Andrew Davis
;;;; Created on 8/27/2019
;;;; MIT licensed

;;; enter the lest package
(in-package lest)

;;; define variables

;;; this variable contains the name of the current test being run
(defvar *test-name* nil)

;;; end of file
