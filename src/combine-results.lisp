;;;; combine-results.lisp
;;;; Defines a macro that combines test results
;;;; Created by Andrew Davis
;;;; Created on 8/27/2019
;;;; MIT licensed

;;; enter the lest package
(in-package lest)

;;; this macro combines the results of multiple test cases
(defmacro combine-results (&body cases)
  "Combines the results (as booleans) of evaluating 'cases' in order"
  (cl-utils:with-gensyms (result)
	`(let ((,result t))
	   ,@(loop for c in cases collect `(unless ,c (setf ,result nil)))
	   ,result)))

;;; end of file
