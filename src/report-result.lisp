;;;; report-result.lisp
;;;; Defines a function that reports results of test cases
;;;; Created by Andrew Davis
;;;; Created on 8/27/2019
;;;; MIT licensed

;;; enter the lest package
(in-package lest)

;;; this function reports the result of a test case
(defun report-result (result form)
  "Reports the result of a single test case. Called by 'check'."
  (format t "~:[FAIL~;pass~] ... ~a: ~a~%" result *test-name* form)
  result)

;;; end of file
