;;;; deftest.lisp
;;;; Defines a macro that creates a test case for lest
;;;; Created by Andrew Davis
;;;; Created on 8/27/2019
;;;; MIT licensed

;;; enter the lest package
(in-package lest)

;;; this macro creates a test function
(defmacro deftest (name parameters &body body)
  "Defines a test function. Within a test function
   we can call other test functions or use 'check'
   to run individual test cases."
  `(defun ,name ,parameters 
     (let ((*test-name* (append *test-name* (list ',name))))
       ,@body)))

;;; end of file
