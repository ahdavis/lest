;;;; check.lisp
;;;; Defines a macro that checks test cases
;;;; Created by Andrew Davis
;;;; Created on 8/27/2019
;;;; MIT licensed

;;; enter the lest package
(in-package lest)

;;; this macro evaluates a group of test cases
(defmacro check (&body forms)
  "Runs each expression in 'forms' as a test case"
  `(combine-results
     ,@(loop for f in forms collect `(report-result ,f ',f))))

;;; end of file
