;;;; lest.asd
;;;; ASDF config file for lest
;;;; Created by Andrew Davis
;;;; Created on 8/27/2019
;;;; MIT licensed

;;; define the system
(asdf:defsystem :lest
 :description "Unit testing framework for Common Lisp"
 :version "1.0"
 :author "Andrew Davis"
 :license "MIT"
 :serial t
 :depends-on (:cl-utils)
 :components ((:module "src"
		:components ((:file "packages")
				(:file "utils")
				(:file "vars" :depends-on ("packages"))
				(:file "deftest" :depends-on ("packages" "vars"))
				(:file "report-result" :depends-on ("packages" "vars"))
				(:file "combine-results" :depends-on ("packages"))
				(:file "check" :depends-on ("packages" "report-result" "combine-results"))))))

;;; end of file
